#!/bin/sh

MYDIR=`dirname "$0"`
ABSDIR=`realpath "$MYDIR"`

MOSYNC_SRCDIR=${ABSDIR%/${ABSDIR#*/MoSyncSrc-*/}}

MOSYNC_VERSION=`basename "$MOSYNC_SRCDIR"`
MOSYNC_VERSION=${MOSYNC_VERSION#*-}

case "$MOSYNCDIR" in
  *$MOSYNC_VERSION*) ;;
  *)
    echo "This MoSyncSrc version is ${MOSYNC_VERSION} but MOSYNCDIR is $MOSYNCDIR!" 1>&2
    exit 1
  ;;
esac

cd "$MYDIR"

ruby RuntimeBuilder.rb ./Settings.rb android "$MOSYNCDIR/profiles/runtimes/android_17/1/"
