# Copyright (C) 2009 Mobile Sorcery AB
# 
# This program is free software; you can redistribute it and/or modify it under
# the terms of the GNU General Public License, version 2, as published by
# the Free Software Foundation.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
# for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; see the file COPYING.  If not, write to the Free
# Software Foundation, 59 Temple Place - Suite 330, Boston, MA
# 02111-1307, USA.

if ENV['OS'] == "Windows_NT" or ENV['OS'] == "cygwin"

	$SETTINGS = {
		:java_source => "../../runtimes/java/",
		:symbian_source => "../../runtimes/cpp/platforms/symbian/",
		:wince_source => "../../runtimes/cpp/platforms/winmobile/",

		:javame_sdk => "C:\\SonyEricsson\\JavaME_SDK_CLDC\\OnDeviceDebug\\",
		:android_sdk => "C:/adt-bundle-windows/sdk",
		:android_ndk => "c:/android-ndk-r9d",
		:s60v5_compiler => "\\Program\\CSL Arm Toolchain\\bin\\",

	
		:verbose => false,
	}
else

	$SETTINGS = {
		:java_source => "../../runtimes/java/",
		:symbian_source => "../../runtimes/cpp/platforms/symbian/",
		:wince_source => "../../runtimes/cpp/platforms/winmobile/",

		:javame_sdk => "/home/runtimebuilder/SDK/SonyEricsson/JavaME_SDK_CLDC/OnDeviceDebug/",
		:android_sdk => "/opt/adt-bundle-linux-x86_64/sdk",
		:android_ndk => "/opt/android-ndk-r9d",
		:s60v5_compiler => "\\Program\\CSL Arm Toolchain\\bin\\",

	
		:verbose => true,
	}
end
